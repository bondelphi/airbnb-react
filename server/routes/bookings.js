const express = require('express');
const router = express.Router();
// const Booking = require('../models/booking');
const UserController = require('../controllers/userController');
const BookingController = require('../controllers/bookingController');

router.post('', UserController.authMiddleware, BookingController.createBooking);

module.exports = router;
