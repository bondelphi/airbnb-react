const User = require('../models/user');
const { normalizeErrors } = require('../helpers/mongoose');
const jwt = require('jsonwebtoken');
const config = require('../config/dev');

exports.auth = function(req, res) {
  const { email, password } = req.body;

  if (!password || !email) {
    return res.status(422).send({
      errors: [
        { title: 'Data missing!', detail: 'Provide email and password!' }
      ]
    });
  }

  User.findOne({ email }, function(err, existingUser) {
    if (err) {
      return res.status(422).send({ errors: normalizeErrors(err.errors) });
    }

    if (!existingUser) {
      return res.status(422).send({
        title: 'Invalid email',
        detail: 'User does not exist'
      });
    }

    if (existingUser.hasSamePassword(password)) {
      const token = jwt.sign(
        {
          userId: existingUser.id,
          username: existingUser.username
        },
        config.SECRET,
        { expiresIn: '1h' }
      );
      return res.json(token);
    } else {
      return res.status(422).send({
        errors: [{ title: 'Wrong data!', detail: 'Wrong email or password!' }]
      });
    }
  });
};

exports.register = function(req, res) {
  // const username = req.body.username;
  // const email = req.body.email;
  // const password = req.body.password;
  // const passwordConfirmation = req.body.passwordConfirmation;
  // or write code in one line
  const { username, email, password, passwordConfirmation } = req.body;

  if (!password || !email) {
    return res.status(422).send({
      errors: [
        { title: 'Data missing!', detail: 'Provide email and password!' }
      ]
    });
  }

  if (password !== passwordConfirmation) {
    return res.status(422).send({
      errors: [
        {
          title: 'Invalid password!',
          detail: 'Password is not same as confirmation!'
        }
      ]
    });
  }

  // User.findOne({email: email})
  // or
  User.findOne({ email }, function(err, existingUser) {
    if (err) {
      return res.status(422).send({ errors: normalizeErrors(err.errors) });
    }

    if (existingUser) {
      return res.status(422).send({
        title: 'Invalid email!',
        detail: 'User with this email already exist'
      });
    }

    const user = new User({
      username,
      email,
      password
    });
    user.save(function(err) {
      if (err) {
        return res.status(422).send({ errors: normalizeErrors(err.errors) });
      }

      return res.json({ registered: true });
    });
  });
};

exports.authMiddleware = function(req, res, next) {
  const token = req.headers.authorization;
  if (token) {
    const user = parseToken(token);
    User.findById(user.userId, function(err, user) {
      if (err) {
        return res.status(422).send({ errors: normalizeErrors(err.errors) });
      }

      if (user) {
        res.locals.user = user;
        next();
      } else {
        return notAuthorized(res);
      }
    });
  } else {
    return notAuthorized(res);
  }
};

function parseToken(token) {
  // header data look like this 'Bearer sdasdsfsafsfasfyteiyohgnnngnd'
  return jwt.verify(token.split(' ')[1], config.SECRET);
}

function notAuthorized(res) {
  return res.status(401).send({
    errors: [
      {
        title: 'Not authorized!',
        detail: 'You need to login to get access!'
      }
    ]
  });
}
