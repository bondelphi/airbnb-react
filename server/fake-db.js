const Rental = require('./models/rental');
const User = require('./models/user');
const Booking = require('./models/booking');

class fakeDb {
  constructor() {
    this.rentals = [
      {
        title: 'Modern apartment',
        city: 'San Francisco',
        street: 'Main Street',
        category: 'apartment',
        image:
          'https://thespaces.com/wp-content/uploads/2016/05/Vertigo-Taylor-Street-San-Fran-7-FT.jpg',
        bedrooms: 3,
        shared: true,
        description: 'Very nice apartment in center of the city.',
        dailyRate: 45
      },
      {
        title: 'Modern Condo',
        city: 'New York',
        street: 'Time Square',
        category: 'condo',
        image:
          'https://ds1.cityrealty.com/img/0f938b6525d4734d74d8a24aad2b9cbf67d8f6d4+w+h+0+60/988-fifth-avenue-00.jpg',
        bedrooms: 3,
        shared: false,
        description: 'Very nice condo in center of the city.',
        dailyRate: 25
      },
      {
        title: 'LUXURY Apt near the BEACH',
        city: 'santa monica',
        street: '6th street',
        category: 'house',
        image:
          'https://q-ak.bstatic.com/images/hotel/max1024x768/213/213369189.jpg',
        bedrooms: 5,
        shared: true,
        description: 'Very nice apartment in center of the city.',
        dailyRate: 72
      },
      {
        title: 'The Stanford West',
        city: 'Carroll',
        street: '24219 US-71',
        category: 'apartment',
        image:
          'https://r-ak.bstatic.com/images/hotel/max1024x768/211/211733610.jpg',
        bedrooms: 2,
        shared: true,
        description: 'Very nice apartment in center of the city.',
        dailyRate: 27
      },
      {
        title: 'LUXURY Apt near the BEACH ',
        city: 'Los Angeles',
        street: 'Wilshire Blvd',
        category: 'apartment',
        image:
          'https://q-ak.bstatic.com/images/hotel/max1024x768/213/213369189.jpg',
        bedrooms: 2,
        shared: true,
        description: 'Very nice apartment in center of the city.',
        dailyRate: 15
      },
      {
        title: 'Westwood Condo',
        city: 'Los Angeles',
        street: 'Esplanade Way',
        category: 'condo',
        image: 'http://photos.heropm.com/file.photos/US/FL/FL0121820L.1.jpg',
        bedrooms: 3,
        shared: false,
        description: 'Very nice condo in center of the city.',
        dailyRate: 20
      },
      {
        title: 'Summit Village',
        city: 'Casselberry, Florida',
        street: 'Esplanade Way',
        category: 'house',
        image: 'http://photos.heropm.com/file.photos/US/FL/FL0121820L.1.jpg',
        bedrooms: 4,
        shared: true,
        description: 'Very nice apartment in center of the city.',
        dailyRate: 30
      }
    ];

    this.users = [
      {
        username: 'test',
        email: 'test@gmail.com',
        password: 'test'
      }
      // ,
      // {
      //   username: 'test2',
      //   email: 'test2@gmail.com',
      //   password: 'test2'
      // },
      // {
      //   username: 'test3',
      //   email: 'test3@gmail.com',
      //   password: 'test3'
      // },
      // {
      //   username: 'test4',
      //   email: 'test4@gmail.com',
      //   password: 'test4'
      // },
      // {
      //   username: 'test5',
      //   email: 'test5@gmail.com',
      //   password: 'test5'
      // }
    ];

    
  }

  async cleanDb() {
    await User.deleteMany({});
    await Rental.deleteMany({});
  }

  pushDataToDb() {
    const user = new User(this.users[0]);
    this.rentals.forEach(rental => {
      const newRental = new Rental(rental);

      newRental.user = user;

      user.rentals.push(newRental);

      newRental.save();
    });
    user.save();
  }

  pushDataToDb2() {
    const booking = new Booking({"startAt": "2019-12-01", "endAt": "2019-12-05", "totalPrice": 3500, "days":5, "guests": 3, "rentals": {"_id": "sdasdas"}, "users": {"_id": "asdsa"}});
    booking.save();
  }


  async seedDb() {
    // await this.cleanDb();
    // await this.pushDataToDb2();
  }
}

module.exports = fakeDb;
