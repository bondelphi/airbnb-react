import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { BwmInput } from '../shared/form/BwmInput';
import { BwmResError } from '../shared/form/BwmResError';
import { required, minLength4 } from '../shared/form/validators'; 

const LoginForm = props => {
  const { handleSubmit, pristine, submitting, submitCallback, valid, errors } = props;

  return (
    <form onSubmit={handleSubmit(submitCallback)}>
      <Field
        label='Email'
        name='email'
        // component='input'
        component={BwmInput}
        type='email'
        className='form-control'
        validate={[required, minLength4]}
      />

      <Field
        label='Password'
        name='password'
        // component='input'
        component={BwmInput}
        type='password'
        className='form-control'
        validate={[required]}
      />

      <button
        type='submit'
        className='btn btn-bwm btn-form'
        disabled={!valid || pristine || submitting}
      >
        Login
      </button>
      {
        <BwmResError errors={errors}/>
      }
    </form>
  );
};

const validate = values => {
  const errors = [];

  if (!values.email) {
    errors.email = 'Please enter email!';
  }

  if (!values.password) {
    errors.password = 'Please enter password!';
  }

  return errors;
};

export default reduxForm({
  form: 'loginForm', // a unique identifier for this form
  validate
})(LoginForm);
