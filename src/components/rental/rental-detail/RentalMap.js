import React, { Component } from 'react';
import { MapWithGeocode } from '../../map/GoogleMap';

export class RentalMap extends Component {

  render() {
    const location = this.props.location; 
    return (
        <MapWithGeocode
        googleMapURL='https://maps.googleapis.com/maps/api/js?key=AIzaSyC4R6AN7SmujjPUIGKdyao2Kqitzr1kiRg=3.exp&libraries=geometry,drawing,places'
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `100%` }} />}
        mapElement={<div style={{ height: `100%` }} />}
        location={location}
      />
    )
  }
}
