import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { BwmInput } from '../shared/form/BwmInput';
import { BwmResError } from '../shared/form/BwmResError';

const RegisterForm = props => {
  const {handleSubmit, pristine, submitting, submitCallback, valid, errors} = props;

  return (
    <form onSubmit={handleSubmit(submitCallback)}>
      <Field
        label='Username'
        name='username'
        // component='input'
        component={BwmInput}
        type='text'
        className='form-control'
      />

      <Field
        label='Email'
        name='email'
        // component='input'
        component={BwmInput}
        type='email'
        className='form-control'
      />

      <Field
        label='Password'
        name='password'
        // component='input'
        component={BwmInput}
        type='password'
        className='form-control'
      />

      <Field
        label='Confirmation Password'
        name='passwordConfirmation'
        // component='input'
        component={BwmInput}
        type='password'
        className='form-control'
      />

      <button
        type='submit'
        className='btn btn-bwm btn-form'
        disabled={!valid || pristine || submitting}
      >
        Register
      </button>
      {
        <BwmResError errors={errors}/>
      }
     </form>
  );
};

const validate = values => {
  const errors = {};

  if (values.username && values.username.length < 4) {
    errors.username = 'Username min length is 4 characters!';
  }

  if (!values.email) {
    errors.email = 'Please enter email!';
  }

  if (!values.password) {
    errors.password = 'Please enter password!';
  }

  if (!values.passwordConfirmation) {
    errors.passwordConfirmation = 'Please enter password confirmation!';
  }

  if (values.password !== values.passwordConfirmation) {
    errors.passwordConfirmation = 'Password not match!';
  }

  //   if (!values.username) {
  //     errors.username = 'Required';
  //   } else if (values.username.length > 15) {
  //     errors.username = 'Must be 15 characters or less';
  //   }
  //   if (!values.email) {
  //     errors.email = 'Required';
  //   } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
  //     errors.email = 'Invalid email address';
  //   }
  //   if (!values.age) {
  //     errors.age = 'Required';
  //   } else if (isNaN(Number(values.age))) {
  //     errors.age = 'Must be a number';
  //   } else if (Number(values.age) < 18) {
  //     errors.age = 'Sorry, you must be at least 18 years old';
  //   }
  return errors;
};

export default reduxForm({
  form: 'registerForm', // a unique identifier for this form
  validate
})(RegisterForm);
