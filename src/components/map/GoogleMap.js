import React, { Component } from 'react';
import { Cacher } from '../../services/cacher';

import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  // Marker,
  Circle,
  InfoWindow
} from 'react-google-maps';
import { Promise } from 'q';

function MapComponent(props) {
  const { coordinates, isError, isLocationLoaded } = props;
  // const coordinates = props.coordinates;

  return (
    <GoogleMap
      defaultZoom={13}
      defaultCenter={coordinates}
      center={coordinates}
      options={{ disableDefaultUI: isError ? true : false }}
    >
      {isLocationLoaded && !isError && (
        <Circle center={coordinates} radius={500} />
      )}
      {/* <Marker position={coordinates} /> */}

      {isLocationLoaded && isError && (
        <InfoWindow position={coordinates} option={{ maxWidth: 300 }}>
          <div>
            Upppppps, there is problem to find location on the map, we are
            trying to resolve problem as fast as possible. Contract host for
            additional informations if you are still interested in booking this
            place. We are sorry for incoviniance.
          </div>
        </InfoWindow>
      )}
    </GoogleMap>

    // <GoogleMap
    //   defaultZoom={8}
    //   defaultCenter={{ lat: coordinates.lat, lng: coordinates.lng }}
    // >
    //   <Marker position={{ lat: coordinates.lat, lng: coordinates.lng }} />
    // </GoogleMap>
  );
}

function withGeocode(WrappedComponent) {
  return class extends Component {
    constructor() {
      super();
      this.cacher = new Cacher();
      this.state = {
        coordinates: {
          lat: 0,
          lng: 0
        },
        isError: false,
        isLocationLoaded: false
      };
    }

    componentWillMount() {
      this.getGeocodeLocation();
    }

    updateCoordinates(coordinates) {
      this.setState({
        coordinates,
        isLocationLoaded: true
      });
    }

    getcodeLocation(location) {
      const geocoder = new window.google.maps.Geocoder();
      return new Promise((resolve, reject) => {
        geocoder.geocode({ address: location }, (result, status) => {
          if (status === 'OK') {
            const geometry = result[0].geometry.location;
            const coordinates = { lat: geometry.lat(), lng: geometry.lng() };

            //or
            //const coordinates = { lat: result.lat, lng: result.lng };
            this.cacher.cacheValue(location, coordinates);

            resolve(coordinates);

            //this.setState({ coordinates: this.coordinates});
            //or
            //this.setState({ coordinates });
          } else {
            reject('Error get code location !!!');
          }
        });
      });
    }

    getGeocodeLocation() {
      //for test error
      // let location = this.props.location;
      // if (Math.floor(Math.random() * 10) > 5){
      //   location = 'z/z/z/z/z/z/z/z/z/z/z/!@#$%^&*';
      // }

      // use this
      const location = this.props.location;

      if (this.cacher.isValueCached(location)) {
        this.updateCoordinates(this.center.getCachedValue(location));
        // this.setState({
        //   coordinates: this.center.getCachedValue(location),
        //   isLocationLoaded: true
        // });
      } else {
        this.getcodeLocation(location).then(
          coordinates => {
            this.updateCoordinates(coordinates);
            // this.setState({ coordinates, isLocationLoaded: true });
          },
          error => {
            this.setState({ isError: true, isLocationLoaded: true });
          }
        );
      }
    }

    render() {
      return <WrappedComponent {...this.state} />;
    }
  };
}

//hoc
export const MapWithGeocode = withScriptjs(
  withGoogleMap(withGeocode(MapComponent))
);

// export const MapWithAMarker = withScriptjs(
//   withGoogleMap(props => (
//     <GoogleMap
//       defaultZoom={8}
//       defaultCenter={{ lat: 38.89511, lng: -77.03637 }}
//     >
//       <Marker position={{ lat: 38.89511, lng: -77.03637 }} />
//     </GoogleMap>
//   ))
// );
