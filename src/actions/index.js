import {
  FETCH_RENTALS_SUCCESS,
  FETCH_RENTAL_BY_ID_INITIAL,
  FETCH_RENTAL_BY_ID_SUCCESS,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT
} from './types';
import axios from 'axios';
import authService from '../services/auth-service';
import axiosService from '../services/axios-service';

const axiosInstance = axiosService.getInstance(); 

const fetchRentalByIdInit = rental => {
  return {
    type: FETCH_RENTAL_BY_ID_INITIAL,
    rental
  };
};

const fetchRentalsSuccess = rentals => {
  return {
    type: FETCH_RENTALS_SUCCESS,
    rentals
  };
};

const fetchRentalByIdSuccess = rental => {
  return {
    type: FETCH_RENTAL_BY_ID_SUCCESS,
    rental
  };
};

export const fetchRentals = () => {
  return dispatch => {
    //new
    axiosInstance
    .get('/rentals')
    .then(res => {
      return res.data;
    })
    .then(rentals => {
      dispatch(fetchRentalsSuccess(rentals));
    });
    
    //old
    // axios
    //   .get('/api/v1/rentals')
    //   .then(res => {
    //     return res.data;
    //   })
    //   .then(rentals => {
    //     dispatch(fetchRentalsSuccess(rentals));
    //   });
  };
};

export const fetchRentalById = rentalId => {
  return function(dispatch) {
    dispatch(fetchRentalByIdInit());
    axios
      .get(`/api/v1/rentals/${rentalId}`)
      .then(res => {
        return res.data;
      })
      .then(rental => {
        dispatch(fetchRentalByIdSuccess(rental));
      });
  };
};

// AUTH ACTIONS

const loginSuccess = token => {
  return {
    type: LOGIN_SUCCESS
  };
};

const loginFailure = errors => {
  return {
    type: LOGIN_FAILURE,
    errors
  };
};

export const register = userData => {
  return axios
    .post('/api/v1/users/register', { ...userData })
    .then(res => res.data, err => Promise.reject(err.response.data));
};

export const checkAuthState = () => {
  return dispatch => {
    if (authService.isAuthenticated()) {
      dispatch(loginSuccess());
    }
  };
};

export const login = userData => {
  return dispatch => {
    return axios
      .post('/api/v1/users/auth', { ...userData })
      .then(res => res.data)
      .then(token => {
        authService.saveToken(token);
        dispatch(loginSuccess());
      })
      .catch(err => {
        dispatch(loginFailure(err.response.data.errors));
      });
  };
};

export const logout = () => {
  authService.removeToken();
  return { type: LOGOUT };
};
